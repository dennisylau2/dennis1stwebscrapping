from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.firefox.service import Service
from webdriver_manager.firefox import GeckoDriverManager
import time

driver = webdriver.Firefox(service=Service(GeckoDriverManager().install()))
driver.get("https://www.foodpanda.hk/restaurants/new?lat=22.318404033806637&lng=114.17000020078622&vertical=restaurants&expedition=delivery")
t_end = time.time() + 60

while True:
    driver.execute_script("window.scrollBy(0, 400)")
    time.sleep(10)
    print("10secs")
    if time.time() > t_end:
        break

all_restaurants = driver.find_elements(By.CSS_SELECTOR, ".vendor-list > .vendor-tile-wrapper")

r_list = []

for r in all_restaurants:
        r_name = r.find_element(By.CSS_SELECTOR, ".headline > .name.fn")
        r_time = r.find_element(By.CSS_SELECTOR, ".badge-info")
        r_rating = r.find_element(By.CSS_SELECTOR, ".ratings-component > .rating")
        r_reviews = r.find_element(By.CSS_SELECTOR, ".ratings-component > .reviews")
        r_fee = r.find_element(By.CSS_SELECTOR, "[data-testid=delivery-fee__content] > strong")
        r_info = [r_name.text, r_time.text.replace("\nMIN", ""), r_rating.text.replace("/", ""), r_reviews.text.replace("(", "").replace(")", ""), r_fee.text.replace("HK$ ", "")]
        r_list.append(r_info)

import pandas as pd
rcsv = pd.DataFrame(r_list, columns=["Restaurant Name", "Delivery Time (min)", "Rating", "Reviews", "Delivery Fee (HKD)"])
rcsv.sort_values("Reviews", ascending=True)
rcsv